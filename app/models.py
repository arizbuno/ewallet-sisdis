from app import db

class User(db.Model):
    user_id = db.Column(db.String(64), primary_key=True)
    nama = db.Column(db.String(64), unique=True)
    saldo = db.Column(db.Integer, default=0)
    
    __table_args__ = (
        db.CheckConstraint(saldo >= 0, name='check_saldo_positive'),
        {})

    def __repr__(self):
        return '<User {}>'.format(self.nama)

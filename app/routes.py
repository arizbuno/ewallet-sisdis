from app import app, api, Resource, parser, db
# from threading import Thread, Lock
from queue import Queue
from requests_futures.sessions import FuturesSession
from .models import User
import concurrent.futures as cf
import os
import sys
import json
import requests
import logging
logging.basicConfig(filename='example.log', level=logging.DEBUG)

SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
JSON_URL = os.path.join(SITE_ROOT, 'static', 'list.json')
USER_ID_HOME = "1506722802"
NAMA_HOME = "Ariz Buditiono"


def check_quorum():
    with open(JSON_URL) as f:
        data = json.load(f)
        
    up_count = 0
    with FuturesSession(max_workers=10) as session:
        futures = []
        for x in range(len(data)):
            logging.info("Establishing connection to " + data[x]['ip'])
            futures.append(session.post('http://' + data[x]['ip'] + '/ewallet/ping'))
        responses = []
        try:
            for future in cf.as_completed(futures, timeout=10):
                responses.append(future.result())
                up_count += 1
        except cf._base.TimeoutError:
            logging.info(data[x]['ip'] + " Failed")

    if up_count <= len(data) / 2:
        return -1
    elif up_count == len(data):
        return 1

    return 0

class GetSaldo(Resource):
    def post(self):
        parser.add_argument('user_id')
        args = parser.parse_args()
        ret = 0
        try:
            if not User.query.filter_by(user_id=args['user_id']).first():
                ret = -1
            elif check_quorum() < 0:
                ret = -2
            elif not User.query.filter_by(user_id=args['user_id']).first():
                if User.query.filter_by(user_id=args['user_id']).first() < 0:
                    ret = -4
            else:
                ret = User.query.filter_by(user_id=args['user_id']).first().saldo
        except:
            logging.warning(sys.exc_info()[0])
            ret = -99
        return {'saldo': ret}

class GetSTotalaldo(Resource):
    def post(self):
        parser.add_argument('user_id')
        args = parser.parse_args()
        ret = -99
        with open(JSON_URL) as f:
            data = json.load(f)

        if args['user_id'] != USER_ID_HOME:
            ip_home = ""
            for branch in data:
                if branch['npm'] == args['user_id']:
                    ip_home = branch['ip']
                    break

            try:
                r = requests.get("http://" + ip_home + "/ewallet/getTotalSaldo")
                return r.json()
            except:
                return {'saldo': ret}

        user = User.query.filter_by(user_id=args['user_id']).first()
        if not user:
            ret = -1
        if not check_quorum() >= 1:
            ret = -2
        with FuturesSession(max_workers=10) as session:
            futures = []
            for x in range(len(data)):
                logging.info("Establishing connection to " + data[x]['ip'])
                futures.append(session.post('http://' + data[x]['ip'] + 'ewallet/getSaldo'))
            
            responses = []
            for future in futures:
                responses.append(future.result())

            total_saldo = 0
            for response in responses:
                saldo = response.json()['saldo']
                if saldo < -1:
                    ret = -3
                    break
                elif saldo == -1:
                    data = {
                        "user_id": USER_ID_HOME,
                        "nama": NAMA_HOME
                    }
                    requests.post(response.url.replace("/getSaldo", "/register"), json=data)
                else:
                    total_saldo += saldo
            ret = total_saldo
        return {'saldo': ret}

class Ping(Resource):
    def post(self):
        return {'pingReturn': 1}

class Transfer(Resource):
    def post(self):
        parser.add_argument('user_id')
        parser.add_argument('nilai')
        args = parser.parse_args()
        args['nilai'] = int(args['nilai'])
        ret = -99
        try:
            user = User.query.filter_by(user_id=args['user_id']).first()
            if not user:
                ret = -1
            elif check_quorum() < 0:
                ret = -2
            elif user.saldo is None:
                ret = -4
            else:
                if args['nilai'] < 0 or args['nilai'] > 1000000000:
                    ret = -5
                else:
                    user.saldo += args['nilai']
                    db.session.commit()
                    ret = 1
        except:
            ret = -99
        return {'transferReturn': ret}

class Register(Resource):
    def post(self):
        parser.add_argument('user_id')
        parser.add_argument('nama')
        args = parser.parse_args()
        ret = -99
        try:
            if check_quorum() < 0:
                ret = -2
            else:
                try:
                    new_user = User(user_id=args['user_id'], nama=args['nama'])
                    db.session.add(new_user)
                    db.session.commit()
                    ret = 1
                except:
                    ret = -4
        except:
            pass
        return {'registerReturn': ret}

class ListQuorum(Resource):
    def get(self):
        with open(JSON_URL) as f:
            data = json.load(f)

        return data

    def post(self):
        with open(JSON_URL) as f:
            data = json.load(f)

        return data

class QuorumEndPoint(Resource):
    def post(self):
        quorum_status = check_quorum()

        return {'status': quorum_status}

api.add_resource(GetSaldo, '/ewallet/getSaldo')
api.add_resource(GetSTotalaldo, '/ewallet/getTotalSaldo')
api.add_resource(Ping, '/ewallet/ping')
api.add_resource(Register, '/ewallet/register')
api.add_resource(Transfer, '/ewallet/transfer')
api.add_resource(ListQuorum, '/ewallet/list')
api.add_resource(QuorumEndPoint, '/ewallet/quorum')